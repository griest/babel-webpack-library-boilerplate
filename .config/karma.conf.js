const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpack.base.config')
const {merge} = require('lodash')

delete webpackConfig.entry
delete webpackConfig.externals
delete webpackConfig.output.libraryTarget

process.env.BABEL_ENV = 'test'

merge(webpackConfig, {
  devtool: 'inline-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      sinon: 'sinon'
    })
  ]
})

module.exports = function (config) {
  config.set({
    basePath: '../test',

    frameworks: ['mocha', 'chai'],

    files: [
      './index.js',
      './assets/*'
    ],

    proxies: {
      '/assets/': '/base/assets/'
    },

    preprocessors: {
      './index.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true,
      stats: 'none'
    },

    reporters: ['html', 'mocha', 'coverage'],

    htmlReporter: {
      outputFile: path.resolve(__dirname, '../test/reports/unit/index.html'),
      pageTitle: 'Pexi Unit Test Report',
      groupSuites: true
    },

    coverageReporter: {
      dir: path.resolve(__dirname, '../test/reports/coverage'),
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    },

    port: 9876,

    colors: true,

    logLevel: config.LOG_WARN,

    autoWatch: false,

    browsers: ['Chrome'],

    customLaunchers: {
      'Chrome_no_security': {
        base: 'Chrome',
        flags: ['--disable-web-security']
      }
    },

    singleRun: true,

    concurrency: Infinity
  })
}
